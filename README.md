#  [Opennode](#) 
```bash 
git clone https://yur333@bitbucket.org/yur333/opennode.git
 ```
  
 ```bash 
 cd opennode
  ```    
  ```bash
  npm install
  ```
   create   `.env` file:
```text
DATABASE_SERVICE_NAME=MONGODB
IP=0.0.0.0
MONGO_URL=27017
MONGODB_SERVICE_HOST=localhost
MONGODB_SERVICE_PORT=27017
MONGODB_DATABASE=test
MONGODB_PASSWORD=
MONGODB_USER=
PORT=8080
```
then you can test it
  ```bash
  npm run dev
  ```

 
# [See it live!](http://opennode-opennode.a3c1.starter-us-west-1.openshiftapps.com/)
