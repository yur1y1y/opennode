import express from 'express';
import load from 'express-load';
import morgan from 'morgan';
import * as    bodyParser from 'body-parser';
import    cookieParser from 'cookie-parser';
import session from 'express-session';
import passport from 'passport'
import  flash from 'connect-flash';
import validator from 'express-validator';

const MongoStore = require('connect-mongo')(session);
import csrf from 'csurf';
import * as dotenv from 'dotenv';
import {createFakeCategories, createFakeProducts, configurePassport, runApp, connectMongo} from './helpers/startup';
import {getUrl} from "./helpers/utils";

//load .env configs
     dotenv.load({path: '.env'});

export const app = express()



connectMongo()


app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// logging
app.use(morgan('dev'));

app.use(validator());
app.use(cookieParser());
app.use(csrf({cookie:true}));

app.use(flash());
app.use(session({
    secret:'somesecret',
    resave:false,
    saveUninitialized:false,
    store: new MongoStore({mongooseConnection:global.db}),
    cookie:{maxAge:1000*60*60*24*15} //15 days
}));

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions



app.use((req,res,next)=> {
    res.locals.message =req.flash('error');
    res.locals.user = req.user;
    res.locals.session =req.session ;
    res.locals.url =getUrl(req);
    res.locals.csrfToken= req.csrfToken()
    next()
})



// Autoload scripts (routes, controllers, models...) into an object
// - great for large Node applications.
    load('helpers')
        .then('models')
        .then('controllers')
        .then('routes')
        .into(app);

configurePassport()
createFakeCategories();
createFakeProducts();

runApp()

// for tests
module.exports = app;