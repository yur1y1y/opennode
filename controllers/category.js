import {getUrl} from '../helpers/utils';

module.exports = (app) => {

    const CategoryModel = app.models.category,
        ProductModel = app.models.product,

        categoryController = {

            createFakes: (data) => {

                CategoryModel.countDocuments({}, function (err, count) {
                    if (count === 0) {
                        const categories = require(data),
                            newCategories = [],
                            exactCategory = (categories) => {
                                for (let i = 0; i < categories.length; i++) {
                                    delete categories[i]['_id'];
                                    if (categories[i]['categories']) {
                                        const nestedCategories = categories[i]['categories'];
                                        delete categories[i]['categories'];
                                        newCategories.push(categories[i]);
                                        exactCategory(nestedCategories);
                                    }
                                }
                            };
                        exactCategory(categories);

                        newCategories.forEach(cat => {
                            cat = CategoryModel(cat);

                            cat.save(err => {
                                if (err) {
                                    console.log('shit', err)
                                }

                                else {
                                    console.log('success')
                                }
                            })
                        });
                    }
                })
            },
            rootCategories: (req, res) => {

                CategoryModel.find({parent_category_id: 'root'})
                    .exec((err, root_category) => {

                        if (!err ) {

                            res.render('main/index', {status:200,root_category});

                        }
                    });

            },
            subCategories: (req, res) => {
                let root_category, selected_category;

                CategoryModel.find({parent_category_id: 'root'})
                    .exec((err, root) => {
                        if (!err) {

                            root_category = root;

                            root_category.forEach(r => {
                                if (r.id === req.params.category) {
                                    selected_category = r
                                }

                            })

                            CategoryModel.find({parent_category_id: req.params.category})
                                .exec((err, sub_category) => {

                                    if (!err) {
                                        sub_category.forEach(sub => {
                                            if (!sub.image) {
                                                const image = 'categories/' + sub.id + '.jpg';
                                                Object.assign(sub, {image})
                                            }
                                        });

                                        res.render('category/sub', {
                                            status:200,
                                            root_category,
                                            sub_category,
                                            selected_category
                                        });
                                    }
                                });
                        }
                    });

            },
            exactCategory: (req, res) => {

                let root_category = [], selected_category, exact_category;

                CategoryModel.find({})
                    .exec((err, cat) => {
                        if (!err) {

                            cat.forEach(c => {
                                if (c.parent_category_id === 'root') {
                                    root_category.push(c)
                                }
                                if (c.id === req.params.category) {
                                    selected_category = c

                                }

                                if (c.id === req.params.exact_category) {
                                    exact_category = c
                                }

                            });

                            ProductModel.find({primary_category_id:{$regex:'^' +req.params.exact_category}})
                                .exec((err, products) => {

                                    if (!err) {
                                        res.render('category/exact', {
                                            status:200,
                                            root_category,
                                            exact_category,
                                            selected_category,
                                            products
                                        });
                                    }
                                })
                        }
                    });
            }
        };


    return categoryController
};