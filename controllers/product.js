import slug from 'limax';

module.exports = (app) => {

    const ProductModel = app.models.product,
        CategoryModel = app.models.category,
        UserModel =app.models.user,
        productController = {

            createFakes: (data) => {

                ProductModel.countDocuments({}, function (err, count) {
                    if (count === 0) {
                        const products = require(data);
                        const urls = [], uniqueUrls = (url, i) => {
                            let url_end = 1;

                            if (i) {
                                url_end = i;
                            }

                            if (urls.indexOf(slug(url)) === -1) {
                                urls.push(slug(url))
                                return slug(url)
                            } else {
                                urls.push(slug(url));

                                if (i) url = url.slice(0, -2);
                                return uniqueUrls(slug(url + '-' + url_end), url_end + 1)
                            }
                        }
                        products.forEach((pro, i) => {

                            delete products[i]['_id'];

                            Object.assign(pro, {url: uniqueUrls(pro.name)});
                            pro = ProductModel(pro);

                            pro.save(err => {
                                if (err) {
                                    console.log('shit', err)
                                }

                                else {
                                    console.log('success')
                                }
                            })
                        });

                    }
                })
            },
            getProduct: (req, res) => {
                let root_category = [], selected_category, exact_category;

                CategoryModel.find({})
                    .exec((err, cat) => {
                        if (!err) {

                            cat.forEach(c => {
                                if (c.parent_category_id === 'root') {
                                    root_category.push(c)
                                }
                                if (c.id === req.params.category) {
                                    selected_category = c

                                }

                                if (c.id === req.params.exact_category) {
                                    exact_category = c
                                }

                            });

                            ProductModel.findOne({url: req.params.product_url})
                                .exec((err, product) => {


                                    if (!err) {
                                        if(req.user) {
                                            if (req.user.viewed.indexOf(product._id) === -1 || !req.user.viewed) {
                                                UserModel.update({_id: req.user._id},
                                                    {$push: {viewed: product._id}}, (err, user) => {
                                                    //callback
                                                })
                                            }
                                        }

                                        res.render('product/product', {
                                            status: 200,
                                            root_category,
                                            selected_category,
                                            exact_category,
                                            product
                                        })
                                    }

                                })
                        }
                    });
            }
        }
    return productController
}