module.exports = (app) => {
    const ProductModel = app.models.product,
        CategoryModel =app.models.category,
        mainController = {
            index: (req, res) => {
                res.redirect('/store')
            },
            productSearch: (req, res) => {

                let root_category = [];
                CategoryModel.find({})
                    .exec((err, cat) => {
                        if (!err) {

                            cat.forEach(c => {
                                if (c.parent_category_id === 'root') {
                                    root_category.push(c)
                                }

                            });

                            ProductModel.find({
                                name: {$regex:'.*'+req.query.q+ '.*',$options: 'i'}
                            }).exec((err, products) => {

                                if (!err) {

                                    if(products)  {
                                        products.forEach(pro => {
                                            const mainCategory = pro.primary_category_id.slice(0, pro.primary_category_id.indexOf('-')),
                                                secondCategory =pro.primary_category_id.slice(0, pro.primary_category_id.lastIndexOf('-'));
                                            pro.urlToProduct = '/store/' + mainCategory + '/' + secondCategory + '/' + pro.url

                                        })

                                          res.render('main/search', {
                                            status: 200,
                                            root_category,
                                            products,
                                              query:req.query.q
                                       });
                                    }
                                }
                            })
                        }
                    });
            },

        }
    return mainController
}