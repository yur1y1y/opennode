import passport from 'passport';
import crypto from 'crypto';
import {emailResetedTemplate, emailResetTemplate, emailVerifyTemplate, emailWishListTemplate} from "../helpers/utils";

const LocalStrategy = require('passport-local').Strategy;

module.exports = (app) => {

    const UserModel = app.models.user,
        CategoryModel = app.models.category,
        TokenModel = app.models.token,
        ProductModel = app.models.product,
        userController = {
            createLocalStrategy: () => {
                passport.serializeUser(function (user, done) {
                    done(null, user.id);
                });

                // used to deserialize the user
                passport.deserializeUser(function (id, done) {
                    UserModel.findById(id, function (err, user) {
                        done(err, user);
                    });
                });

                // =========================================================================
                // LOCAL SIGNUP ============================================================
                // =========================================================================

                passport.use('local-signup', new LocalStrategy({
                        // by default, local strategy uses username and password, we will override with email
                        usernameField: 'email',
                        passwordField: 'password',
                        passReqToCallback: true // allows us to pass back the entire request to the callback
                    },
                    function (req, email, password, done) {

                        // asynchronous
                        // User.findOne wont fire unless data is sent back
                        process.nextTick(function () {

                            // find a user whose email is the same as the forms email
                            // we are checking to see if the user trying to login already exists
                            UserModel.findOne({'email': email}, function (err, user) {
                                // if there are any errors, return the error
                                if (err)
                                    return done(err);

                                // check to see if theres already a user with that email
                                if (user) {
                                    return done(null, false, req.flash('error', 'That email is already taken.'));
                                } else {

                                    // if there is no user with that email
                                    // create the user
                                    var newUser = new UserModel({
                                        email,
                                        password: UserModel.prototype.generateHash(password),
                                        name: req.body.name || 'John',
                                        surname: req.body.surname || 'Doe'
                                    });

                                    // save the user
                                    newUser.save(function (err, user) {
                                        if (err) {
                                            throw err
                                        }
                                        else {

                                            const token = new TokenModel({
                                                userId: user._id,
                                                token: crypto.randomBytes(16).toString('hex')
                                            });
                                            // Save the verification token
                                            token.save(function (err) {
                                                if (err) {
                                                    req.flash('error', err)
                                                    return res.redirect('/user/signup');
                                                }

                                                const sgMail = require('@sendgrid/mail');
                                                sgMail.setApiKey(process.env.SENDGRID_API_KEY);
                                                const msg = {
                                                    to: user.email,
                                                    from: 'no-reply@opennode.com',
                                                    subject: 'Account Verification Token',
                                                    html: emailVerifyTemplate(req.protocol + '://' + req.headers.host + '/user/verify/' + token.token),
                                                };
                                                sgMail.send(msg);


                                                return done(null, newUser);
                                            });

                                        }
                                    });
                                }

                            });

                        });

                    }));

                passport.use('local-login', new LocalStrategy({
                        // by default, local strategy uses username and password, we will override with email
                        usernameField: 'email',
                        passwordField: 'password',
                        passReqToCallback: true // allows us to pass back the entire request to the callback
                    },
                    function (req, email, password, done) { // callback with email and password from our form

                        // find a user whose email is the same as the forms email
                        // we are checking to see if the user trying to login already exists
                        UserModel.findOne({'email': email}, function (err, user) {
                            // if there are any errors, return the error before anything else
                            if (err)
                                return done(err);

                            // if no user is found, return the message
                            if (!user)
                                return done(null, false, req.flash('error', 'No user found.')); // req.flash is the way to set flashdata using connect-flash

                            // if the user is found but the password is wrong
                            if (!user.validPassword(password))
                                return done(null, false, req.flash('error', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata

                            // all is well, return successful user
                            return done(null, user);
                        });

                    }));

            },
            signupPage: (req, res) => {
                CategoryModel.find({parent_category_id: 'root'})
                    .exec((err, root_category) => {

                        if (!err) {
                            res.render('user/signup', {
                                status: 200,
                                root_category
                            })

                        }
                    });
            },
            signInPage: (req, res) => {
                CategoryModel.find({parent_category_id: 'root'})
                    .exec((err, root_category) => {

                        if (!err) {

                            res.render('user/signin', {
                                status: 200,
                                root_category
                            })

                        }
                    });
            },
            signupLocal: (req, res, next) => {

                passport.authenticate('local-signup', {
                    successRedirect: '/user', // redirect to the secure profile section
                    failureRedirect: '/user/signup', // redirect back to the signup page if there is an error
                    failureFlash: true // allow flash messages
                })(req, res, next)

            },
            signinLocal: (req, res, next) => {
                passport.authenticate('local-login', {
                    successRedirect: '/user', // redirect to the secure profile section
                    failureRedirect: '/user/signin', // redirect back to the signup page if there is an error
                    failureFlash: true // allow flash messages
                })(req, res, next)
            },
            logout: (req, res, next) => {

                req.logout();
                res.redirect('/');
            },
            profilePage: (req, res) => {
                if (!req.user) {
                    res.redirect('/user/signin')
                }

                CategoryModel.find({parent_category_id: 'root'})
                    .exec((err, root_category) => {
                        if (!err) {
                            ProductModel.find({_id: {$in: req.user.viewed}}, (err, products) => {
                                if (err) req.flash('error', err)

                                products.forEach(pro => {
                                    const mainCategory = pro.primary_category_id.slice(0, pro.primary_category_id.indexOf('-')),
                                        secondCategory = pro.primary_category_id.slice(0, pro.primary_category_id.lastIndexOf('-'));
                                    pro.urlToProduct = '/store/' + mainCategory + '/' + secondCategory + '/' + pro.url
                                })

                                res.render('user/profile', {
                                    status: 200,
                                    root_category,
                                    products
                                })
                            })


                        }
                    });
            },
            updateProfile: (req, res) => {
                const name = req.body.name, surname = req.body.surname;
                UserModel.update({_id: req.user._id}, {$set: {name, surname}}).exec((err, user) => {

                    if (err) {
                        req.flash('error', err)
                    }
                    else {
                        res.redirect('/user')
                    }
                })
            },
            verifyUser: (req, res) => {
                TokenModel.findOne({token: req.params.token}, function (err, tok) {
                    if (!tok) {
                        req.flash('error', 'Token is invalid or expired.');
                        res.redirect('/user');
                    }
                    // If we found a token, find a matching user
                    UserModel.findOne({_id: tok.userId}, function (err, user) {
                        if (!user) {
                            req.flash('error', 'We were unable to find a user for this token')
                            res.redirect('/user')
                        }
                        if (user.verified) {
                            res.redirect('/user')
                        }
                        // Verify and save the user
                        user.verified = true;
                        user.save(function (err) {
                            if (err) {
                                req.flash('error', err)
                            }
                            res.redirect('/user')
                        });
                    });
                });
            },
            resendToken: (req, res) => {

                const token = new TokenModel({
                    userId: req.user._id,
                    token: crypto.randomBytes(16).toString('hex')
                });
                // Save the verification token
                token.save(function (err) {
                    if (err) {
                        req.flash('error', err)
                        res.redirect('/user');
                    }

                    const sgMail = require('@sendgrid/mail');
                    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
                    const msg = {
                        to: req.user.email,
                        from: 'no-reply@opennode.com',
                        subject: 'Account Verification Token',
                        html: emailVerifyTemplate(req.protocol + '://' + req.headers.host + '/user/verify/' + token.token),
                    };
                    sgMail.send(msg);
                    res.redirect('/user')
                })
            },
            forgotPasswordPage: (req, res) => {
                CategoryModel.find({parent_category_id: 'root'})
                    .exec((err, root_category) => {

                        if (!err) {
                            res.render('user/forgot', {
                                status: 200,
                                root_category
                            })

                        }
                    });

            },
            forgotPassword: (req, res) => {
                const newPasswordToken = crypto.randomBytes(16).toString('hex');
                UserModel.findOneAndUpdate({email: req.body.email}, {
                    $set: {
                        passwordResetToken: newPasswordToken,
                        passwordResetExpires: new Date().setHours(new Date().getHours() + 3)// 3 hour for reset
                    }
                }, function (err, user) {

                    if (!user) {
                        req.flash('error', 'No account with that email address exists.')
                        res.redirect('user/signup')
                    }

                    const sgMail = require('@sendgrid/mail');
                    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
                    const msg = {
                        to: user.email,
                        from: 'no-reply@opennode.com',
                        subject: 'Account Password Reset ',
                        html: emailResetTemplate(req.protocol + '://' + req.headers.host + '/user/reset-password/' + newPasswordToken),
                    };
                    sgMail.send(msg);
                    res.redirect('/store')
                })


            },
            resetPasswordPage: (req, res) => {
                CategoryModel.find({parent_category_id: 'root'})
                    .exec((err, root_category) => {

                        if (!err) {

                            UserModel.findOne({passwordResetToken: req.params.passwordToken}, function (err, user) {

                                if (user && user.passwordResetExpires) {
                                    const hoursDiff = (dt1, dt2) => Math.abs(Math.round(dt1.getTime() - dt2.getTime()) / (1000 * 60 * 60));
                                    const diff = hoursDiff(user.passwordResetExpires, new Date());


                                    if (diff > 3) {
                                        req.flash('error', 'Password reset token is expired')
                                        res.redirect('/user/forgot')
                                    }
                                }

                                res.render('user/reset-password', {
                                    passwordToken: req.params.passwordToken,
                                    root_category
                                })
                            })
                        }
                    });
            },
            resetPassword: (req, res) => {


                console.log(req.body.password, req.body.passwordToken);
                UserModel.findOne({passwordResetToken: req.body.passwordToken}, function (err, user) {

                    if (!err) {

                        const hoursDiff = (dt1, dt2) => Math.abs(Math.round(dt1.getTime() - dt2.getTime()) / (1000 * 60 * 60));
                        const diff = hoursDiff(user.passwordResetExpires, new Date());

                        if (diff > 3) {
                            req.flash('error', 'Password reset token is expired')
                            res.redirect('/user/forgot')
                        }
                        UserModel.update({passwordResetToken: req.body.passwordToken},
                            {$set: {password: UserModel.prototype.generateHash(req.body.password)}}, function (err, result) {
                                if (!err) {

                                    const sgMail = require('@sendgrid/mail');
                                    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
                                    const msg = {
                                        to: user.email,
                                        from: 'no-reply@opennode.com',
                                        subject: 'Account Password Updated ',
                                        html: emailResetedTemplate('The password for ' + user.name + '  ' + user.surname + ' has updated'),
                                    };
                                    sgMail.send(msg);

                                    res.redirect('/user/signin')
                                }
                            })
                    }
                });
            },
            toWishList: (req, res) => {
                if (req.user) {
                    if (req.user.wishlist.indexOf(req.body._id) === -1 || !req.user.wishlist) {
                        UserModel.update({_id: req.user._id},
                            {$push: {wishlist: req.body._id}}, (err, user) => {

                             ProductModel.findOne({_id:req.body._id},(err,pro) => {

                                pro.productImageUrl = req.protocol + '://' + req.get('host') + '/img/'+pro.image_groups[2].images[0].link;
                                const mainCategory = pro.primary_category_id.slice(0, pro.primary_category_id.indexOf('-')),
                                    secondCategory =pro.primary_category_id.slice(0, pro.primary_category_id.lastIndexOf('-'));
                                pro.urlToProduct = '/store/' + mainCategory + '/' + secondCategory + '/' + pro.url

                                const sgMail = require('@sendgrid/mail'); //email send feature
                                sgMail.setApiKey(process.env.SENDGRID_API_KEY);
                                const msg = {
                                    to: req.user.email,
                                    from: 'no-reply@opennode.com',
                                    subject: 'Wishlist item added',
                                    html: emailWishListTemplate(pro),
                                };
                                sgMail.send(msg);

                                res.redirect('back')

                            });

                            })
                    } else {
                        UserModel.update({_id: req.user._id},
                            {$pull: {wishlist: req.body._id}}, (err, user) => {
                                res.redirect('back')
                            })
                    }
                }
                else {
                    res.redirect('back')
                }
            },
            wishPage: (req, res) => {

                if (req.user) {
                    CategoryModel.find({parent_category_id: 'root'})
                        .exec((err, root_category) => {

                            if (!err) {
                                ProductModel.find({_id: {$in: req.user.wishlist}}, (err, products) => {
                                    if (err) req.flash('error', err)

                                    products.forEach(pro => {
                                        const mainCategory = pro.primary_category_id.slice(0, pro.primary_category_id.indexOf('-')),
                                            secondCategory = pro.primary_category_id.slice(0, pro.primary_category_id.lastIndexOf('-'));
                                        pro.urlToProduct = '/store/' + mainCategory + '/' + secondCategory + '/' + pro.url
                                    })

                                    res.render('user/wishlist', {status: 200, products,root_category})
                                })
                            } else {
                                res.redirect('back')
                            }
                        })
                }
            },
            removeViewed: (req,res) => {
                if(!req.user) res.redirect('user/signin')

                UserModel.update({_id: req.user._id},
                    {$pull: {viewed: req.body._id}}, (err, user) => {
                        res.redirect('back')
                    })
            }
        }

    return userController
}