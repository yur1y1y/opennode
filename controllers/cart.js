module.exports = (app) => {

    const CartModel = require('../models/cart'),
        CategoryModel = app.models.category,
        ProductModel = app.models.product,
        cartController = {
            cartPage: (req, res) => {
                CategoryModel.find({parent_category_id: 'root'})
                    .exec((err, root) => {
                        if (!err) {

                            const cart = req.session.cart, items = [];
                            if (cart.items) {
                                for (let key in cart.items) {
                                    items.push(cart.items[key].item)
                                }
                                ProductModel.find({_id: {$in: items}}).exec((err, products) => {

                                    products.forEach(pro => {

                                        for (let key in cart.items) {
                                            if ('' + pro._id === cart.items[key].item) {
                                                pro.qty = cart.items[key].qty
                                            }
                                        }
                                        const mainCategory = pro.primary_category_id.slice(0, pro.primary_category_id.indexOf('-')),
                                        secondCategory =pro.primary_category_id.slice(0, pro.primary_category_id.lastIndexOf('-'));
                                        pro.urlToProduct = '/store/' + mainCategory + '/' + secondCategory + '/' + pro.url


                                    })

                                    if (err) {
                                        req.flash('error', JSON.stringify(err, null, 2))
                                        return res.redirect('/')
                                    }
                                    else {
                                        // console.log(products[0]);
                                        res.render('user/cart', {root_category: root, cart, products})

                                    }

                                })
                            }
                            else {
                                res.render('user/cart', {root_category: root, cart})

                            }


                        }

                    })
            },
            addToCart: (req, res) => {

                const productId = req.params.id,
                    cart = new CartModel(req.session.cart ? req.session.cart : {});

                ProductModel.findOne({_id: productId}, (err, product) => {
                    if (err) {

                        req.flash('error', JSON.stringify(err, null, 2))
                        return res.redirect('/')
                    }
                    cart.add(product, productId);
                    req.session.cart = cart

                    res.redirect(req.url.replace('/add-to-cart/' + req.params.id, ''))
                })
            },
            searchAddToCart: (req, res) => {

                const productId = req.query.id,
                    cart = new CartModel(req.session.cart ? req.session.cart : {});

                ProductModel.findOne({_id: productId}, (err, product) => {
                    if (err) {

                        req.flash('error', JSON.stringify(err, null, 2))
                        return res.redirect('/')
                    }
                    cart.add(product, productId);
                    req.session.cart = cart

                    res.redirect('back')
                })
            },
            subtractCart: (req, res) => {
                const productId = req.params.id,
                    cart = new CartModel(req.session.cart ? req.session.cart : {});
                cart.subtract(productId);
                req.session.cart = cart;
                res.redirect('/user/cart');
            },
            removeCart: (req,res) => {
                const productId=req.params.id,
                cart=new CartModel(req.session.cart? req.session.cart : {});
                cart.remove(productId);
                req.session.cart=cart;
                res.redirect('/user/cart');
            }
        }

    return cartController
}