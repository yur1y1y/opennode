import {Schema} from 'mongoose';

module.exports = (app) => {

    const token = new Schema({
        userId: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'user'
        },
        token: {
            type: String,
            required: true
        },
        createdAt: {
            type: Date,
            required: true,
            default: Date.now,
            expires: 43200
        }
    })
    return db.model('token', token)
}