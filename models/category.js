import {Schema} from 'mongoose';

module.exports = (app) => {

    const category = Schema({
        c_showInMenu: {type: Boolean,default:true},
        id:{type: String},
        image:{type:String},
        name:{type: String},
        page_description:{type: String},
        page_title:{type: String},
        parent_category_id:{type: String}
    });

    return db.model('category', category)
}