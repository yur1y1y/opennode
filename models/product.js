import {Schema} from 'mongoose';

module.exports = (app) => {

    const product = Schema({
        c_isSale: {type: Boolean},
        currency: {type: String},
        id: {type: String},
        image_groups: [{
            images: [{
                alt: {type: String},
                link: {type: String},
                title: {type: String}
            }],
            variation_value: {type: String},
            view_type: {type: String}
        }],
        long_description: {type: String},
        master: {
            master_id: {type: String},
            orderable: {type: Boolean},
            price: {type: Number}
        },
        name: {type: String},
        orderable: {type: Boolean},
        page_description: {type: String},
        page_title: {type: String},
        price: {type: Number},
        price_max: {type: Number},
        primary_category_id: {type: String},
        short_description: {type: String},
        type: {
            master: {type: Boolean}
        },
        url:{type:String},
        variants: [{
            orderable: {type: Boolean},
            price: {type: Number},
            product_id: {type: String},
            variation_values: {
                color: {type: String},
                size: {type: String}
            }
        }],
        variation_attributes: [{
            id: {type: String},
            name: {type: String},
            values: [{
                orderable: {type: Boolean},
                name: {type: String},
                value: {type: String}
            }]
        }]
    });

    return db.model('product',product)
}