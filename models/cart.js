module.exports = function Cart(oldCart) {
    this.items = oldCart.items || {};
    this.totalQty = oldCart.totalQty || 0;
    this.totalPrice = oldCart.totalPrice || 0;

    this.add = function (item, _id) {
        let storedItem = this.items[_id];
        if (!storedItem) {
            storedItem = this.items[_id] = {item: item._id, qty: 0, price: 0};
        }
        storedItem.qty++;
        storedItem.price = item.price;
        this.totalQty++;
        this.totalPrice += item.price;

    };
    this.subtract = function (_id) {

        this.items[_id].qty--;

        this.totalQty--;
        this.totalPrice -= this.items[_id].price;
        if (this.items[_id].qty === 0) {
            delete this.items[_id];
        }
    };
    this.remove = function (_id) {
        this.totalQty -= this.items[_id].qty;
        this.totalPrice -= this.items[_id].price * this.items[_id].qty;
        delete this.items[_id];
    };

}