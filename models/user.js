import {Schema} from 'mongoose';
var bcrypt   = require('bcrypt-nodejs');

module.exports = (app) => {

    const user = Schema({
        email:{type: String},
        password:{type:String},
        verified: {required:true,type: Boolean,default:false},
        name:{type:String},
        surname:{type:String},
        passwordResetToken:{type:String,required: false},
        passwordResetExpires:{type:Date,required:false},
        wishlist:[{type:String,required:false}],
        viewed:[{type:String,required:false}]
    });
    user.methods.generateHash = function(password) {
        return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    };

// checking if password is valid
    user.methods.validPassword = function(password) {
        return bcrypt.compareSync(password, this.password);
    };

    return db.model('user', user)
}