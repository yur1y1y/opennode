import mongoose from 'mongoose';
import {app} from '../server'


export const connectMongo = () => {

    mongoose.Promise = global.Promise;

    let mongoURL ;

    if(process.env.NODE_ENV === 'development') {

         mongoURL =   process.env.MONGO_URL;

        if (mongoURL == null && process.env.DATABASE_SERVICE_NAME) {

          const   mongoServiceName =   process.env.DATABASE_SERVICE_NAME,

                mongoHost =   process.env[mongoServiceName + '_DEV_SERVICE_HOST'],
                mongoPort =   process.env[mongoServiceName + '_DEV_SERVICE_PORT'],
                mongoDatabase = process.env[mongoServiceName + '_DEV_DATABASE'],
                mongoPassword = process.env[mongoServiceName + '_DEV_PASSWORD'],
                mongoUser = process.env[mongoServiceName + '_DEV_USER'];



        if (!mongoURL) {
            mongoURL = 'mongodb://';
        }

        if (mongoUser && mongoPassword) {
            mongoURL += mongoUser + ':' + mongoPassword + '@';
        }

        mongoURL += mongoHost + ':' + mongoPort + '/' + mongoDatabase;
        }

    }

    if(process.env.NODE_ENV === 'production') {
         mongoURL = process.env.OPENSHIFT_MONGODB_DB_URL ;


        if (mongoURL == null && process.env.DATABASE_SERVICE_NAME) {

          const   mongoServiceName = process.env.DATABASE_SERVICE_NAME.toUpperCase() ,

                mongoHost = process.env[mongoServiceName + '_SERVICE_HOST'] ,
                mongoPort = process.env[mongoServiceName + '_SERVICE_PORT'] ,
                mongoDatabase = process.env[mongoServiceName + '_DATABASE'],
                mongoPassword = process.env[mongoServiceName + '_PASSWORD'],
                mongoUser = process.env[mongoServiceName + '_USER'];




        if (mongoUser && mongoPassword) {
            mongoURL += mongoUser + ':' + mongoPassword + '@';
        }

        mongoURL += mongoHost + ':' + mongoPort + '/' + mongoDatabase;
            console.log(mongoURL,mongoServiceName, mongoHost , mongoPort ,mongoDatabase,  mongoUser ,mongoPassword)

        }
    }

        const options = {
            autoIndex: false, // Don't build indexes
            reconnectTries: 30, // Retry up to 30 times
            reconnectInterval: 500, // Reconnect every 500ms
            poolSize: 10, // Maintain up to 10 socket connections
            // If not connected, return errors immediately rather than waiting for reconnect
            bufferMaxEntries: 0,
            useNewUrlParser: true
        }

        function createConnection (dbURL, options) {
            var db = mongoose.createConnection(dbURL, options);

            db.on('error', function (err) {
                // If first connect fails because mongod is down, try again later.
                // This is only needed for first connect, not for runtime reconnects.
                // See: https://github.com/Automattic/mongoose/issues/5169
                if (err.message && err.message.match(/failed to connect to server .* on first connect/)) {
                    console.log(new Date(), String(err));

                    // Wait for a bit, then try to connect again
                    setTimeout(function () {
                        console.log("Retrying first connect...");
                        db.openUri(dbURL).catch(() => {});
                        // Why the empty catch?
                        // Well, errors thrown by db.open() will also be passed to .on('error'),
                        // so we can handle them there, no need to log anything in the catch here.
                        // But we still need this empty catch to avoid unhandled rejections.
                    }, 20 * 1000);
                } else {
                    // Some other error occurred.  Log it.
                    console.error(new Date(), String(err));
                }
            });

            db.once('open', function () {
                console.log("Connection to db established.");
            });

            return db;
        }

        //connect db
    global.db = createConnection(mongoURL,options);

    //trace errors
    global.db.on('error', (err) =>{
        if(err) throw err;
    });

    // console.log(process.env );

    console.log('Connected to MongoDB at: %s', mongoURL)
}
    , createFakeCategories =  () => {

app.controllers.category.createFakes('../helpers/fakes/categories.json');

}

,createFakeProducts =  () => {

app.controllers.product.createFakes('../helpers/fakes/products.json');

}

,configurePassport = () => {

    app.controllers.user.createLocalStrategy()

},
    runApp =() => {

        let port =  process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT,
            ip   =    process.env.OPENSHIFT_NODEJS_IP ||  process.env.IP;

        app.listen(port, ip, () => {
            console.log('App running on http://%s:%s', ip, port);

        });
        app.use((req,res,next)=> {
            res.locals.user = req.user;
            res.locals.session =req.session;
            next();
        })

    }
