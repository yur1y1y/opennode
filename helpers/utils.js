export const
    getUrl = (req, str) => {
        let arrayUrl = [], splittedUrl = req.originalUrl.split('/');
        if (str) {
            return req.protocol + '://' + req.get('host') + '/' + str;
        }

        splittedUrl.forEach((url, i) => {

            if (url !== '' && url !== 'favicon.ico' && url !== 'store') {
                if (i > 1) {
                    let urlEnd = ''
                    for (let j = 0; j < i; j++) {
                        urlEnd = urlEnd + splittedUrl[j] + '/'
                    }

                    arrayUrl.push({
                        url: req.protocol + '://' + req.get('host') + urlEnd + url,
                        label: url.replace(splittedUrl[i - 1] + '-', '')
                    });
                }
                else {
                    arrayUrl.push({url: req.protocol + '://' + req.get('host') + '/' + url, label: url});
                }
            }
        });
        return arrayUrl;
    },
    isLoggedIn = (req, res, next) => {
        if (req.isAuthenticated()) {
            return next()
        }
        res.redirect('/')
    },
    notLoggedIn = (req, res, next) => {
        if (!req.isAuthenticated()) {
            return next()
        }
        res.redirect('/')
    },
    emailVerifyTemplate = (verify_href) => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n' +
        '<html>\n' +
        '  \n' +
        '  <head>\n' +
        '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\n' +
        '    \n' +
        '  </head>\n' +
        '  \n' +
        '  <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"\n' +
        '  style="margin: 0pt auto; padding: 0px; background:#F4F7FA;">\n' +
        '    <table id="main" width="100%" height="100%" cellpadding="0" cellspacing="0" border="0"\n' +
        '    bgcolor="#F4F7FA">\n' +
        '      <tbody>\n' +
        '        <tr>\n' +
        '          <td valign="top">\n' +
        '            <table class="innermain" cellpadding="0" width="580" cellspacing="0" border="0"\n' +
        '            bgcolor="#F4F7FA" align="center" style="margin:0 auto; table-layout: fixed;">\n' +
        '              <tbody>\n' +
        '                <!-- START of MAIL Content -->\n' +
        '                <tr>\n' +
        '                  <td colspan="4">\n' +
        '                    <!-- Logo start here -->\n' +
        '                    <table class="logo" width="100%" cellpadding="0" cellspacing="0" border="0">\n' +
        '                      <tbody>\n' +
        '                        <tr>\n' +
        '                          <td colspan="2" height="30"></td>\n' +
        '                        </tr>\n' +
        '                        <tr>\n' +
        '                          <td valign="top" align="center">\n' +
        '                            \n' +
        '                          </td>\n' +
        '                        </tr>\n' +
        '                        <tr>\n' +
        '                          <td colspan="2" height="30"></td>\n' +
        '                        </tr>\n' +
        '                      </tbody>\n' +
        '                    </table>\n' +
        '                    <!-- Logo end here -->\n' +
        '                    <!-- Main CONTENT -->\n' +
        '                    <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff"\n' +
        '                    style="border-radius: 4px; box-shadow: 0 2px 8px rgba(0,0,0,0.05);">\n' +
        '                      <tbody>\n' +
        '                        <tr>\n' +
        '                          <td height="40"></td>\n' +
        '                        </tr>\n' +
        '                        <tr style="font-family: -apple-system,BlinkMacSystemFont,&#39;Segoe UI&#39;,&#39;Roboto&#39;,&#39;Oxygen&#39;,&#39;Ubuntu&#39;,&#39;Cantarell&#39;,&#39;Fira Sans&#39;,&#39;Droid Sans&#39;,&#39;Helvetica Neue&#39;,sans-serif; color:#4E5C6E; font-size:14px; line-height:20px; margin-top:20px;">\n' +
        '                          <td class="content" colspan="2" valign="top" align="center" style="padding-left:90px; padding-right:90px;">\n' +
        '                            <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">\n' +
        '                              <tbody>\n' +
        '                                <tr>\n' +
        '                                  <td align="center" valign="bottom" colspan="2" cellpadding="3">\n' +
        '                                    <img alt="OpenNode" width="80" src="https://www.coinbase.com/assets/app/icon_email-e8c6b940e8f3ec61dcd56b60c27daed1a6f8b169d73d9e79b8999ff54092a111.png"\n' +
        '                                    />\n' +
        '                                  </td>\n' +
        '                                </tr>\n' +
        '                                <tr>\n' +
        '                                  <td height="30" &nbsp;=""></td>\n' +
        '                                </tr>\n' +
        '                                <tr>\n' +
        '                                  <td align="center"> <span style="color:#48545d;font-size:22px;line-height: 24px;">\n' +
        '          Verify your email address\n' +
        '        </span>\n' +
        '\n' +
        '                                  </td>\n' +
        '                                </tr>\n' +
        '                                <tr>\n' +
        '                                  <td height="24" &nbsp;=""></td>\n' +
        '                                </tr>\n' +
        '                                <tr>\n' +
        '                                  <td height="1" bgcolor="#DAE1E9"></td>\n' +
        '                                </tr>\n' +
        '                                <tr>\n' +
        '                                  <td height="24" &nbsp;=""></td>\n' +
        '                                </tr>\n' +
        '                                <tr>\n' +
        '                                  <td align="center"> <span style="color:#48545d;font-size:14px;line-height:24px;">\n' +
        '          In order to start using your OpenNode account, you need to confirm your email address.\n' +
        '        </span>\n' +
        '\n' +
        '                                  </td>\n' +
        '                                </tr>\n' +
        '                                <tr>\n' +
        '                                  <td height="20" &nbsp;=""></td>\n' +
        '                                </tr>\n' +
        '                                <tr>\n' +
        '                                  <td valign="top" width="48%" align="center"> <span>\n' +
        '          <a href="' + verify_href + '" style="display:block; padding:15px 25px; background-color:#0087D1; color:#ffffff; border-radius:3px; text-decoration:none;">Verify Email Address</a>\n' +
        '        </span>\n' +
        '\n' +
        '                                  </td>\n' +
        '                                </tr>\n' +
        '                                <tr>\n' +
        '                                  <td height="20" &nbsp;=""></td>\n' +
        '                                </tr>\n' +
        '                                <tr>\n' +
        '                                  <td align="center">\n' +
        '                                    <img src="https://s3.amazonaws.com/app-public/Coinbase-notification/hr.png" width="54"\n' +
        '                                    height="2" border="0">\n' +
        '                                  </td>\n' +
        '                                </tr>\n' +
        '                                <tr>\n' +
        '                                  <td height="20" &nbsp;=""></td>\n' +
        '                                </tr>\n' +
        '                                <tr>\n' +
        '                                  <td align="center">\n' +
        '                                    <p style="color:#a2a2a2; font-size:12px; line-height:17px; font-style:italic;">If you did not sign up for this account you can ignore this email and the account\n' +
        '                                      will be deleted.</p>\n' +
        '                                  </td>\n' +
        '                                </tr>\n' +
        '                              </tbody>\n' +
        '                            </table>\n' +
        '                          </td>\n' +
        '                        </tr>\n' +
        '                        <tr>\n' +
        '                          <td height="60"></td>\n' +
        '                        </tr>\n' +
        '                      </tbody>\n' +
        '                    </table>\n' +
        '                    <!-- Main CONTENT end here -->\n' +
        '                    <!-- PROMO column start here -->\n' +
        '                    <!-- Show mobile promo 75% of the time -->\n' +
        '                    <table id="promo" width="100%" cellpadding="0" cellspacing="0" border="0" style="margin-top:20px;">\n' +
        '                      <tbody>\n' +
        '                        <tr>\n' +
        '                          <td colspan="2" height="20"></td>\n' +
        '                        </tr>\n' +
        '                        <tr>\n' +
        '                          <td colspan="2" align="center"> <span style="font-size:14px; font-weight:500; margin-bottom:10px; color:#7E8A98; font-family: -apple-system,BlinkMacSystemFont,&#39;Segoe UI&#39;,&#39;Roboto&#39;,&#39;Oxygen&#39;,&#39;Ubuntu&#39;,&#39;Cantarell&#39;,&#39;Fira Sans&#39;,&#39;Droid Sans&#39;,&#39;Helvetica Neue&#39;,sans-serif;">Get the latest OpenNode App for your phone</span>\n' +
        '\n' +
        '                          </td>\n' +
        '                        </tr>\n' +
        '                        <tr>\n' +
        '                          <td colspan="2" height="20"></td>\n' +
        '                        </tr>\n' +
        '                        <tr>\n' +
        '                          <td valign="top" width="50%" align="right">\n' +
        '                            <a href="https://itunes.apple.com/us/app/opennode-buy-bitcoin-more/id886427730?mt=8"\n' +
        '                            style="display:inline-block;margin-right:10px;">\n' +
        '                              <img src="https://s3.amazonaws.com/app-public/Coinbase-email/iOS_app.png" height="40"\n' +
        '                              border="0" alt="OpenNode iOS mobile bitcoin wallet">\n' +
        '                            </a>\n' +
        '                          </td>\n' +
        '                          <td valign="top">\n' +
        '                            <a href="https://play.google.com/store/apps/details?id=com.opennode.android&referrer=utm_source%3Dko_c5255a2d591c8dffc%26utm_medium%3D1%26utm_campaign%3Dkoopennode----production553ec3be25c1308daf2a5d2791%26utm_term%3D%26utm_content%3D%26"\n' +
        '                            style="display:inline-block;margin-left:5px;">\n' +
        '                              <img src="https://s3.amazonaws.com/app-public/Coinbase-email/Android_app.png"\n' +
        '                              height="40" border="0" alt="OpenNode Android mobile bitcoin wallet">\n' +
        '                            </a>\n' +
        '                          </td>\n' +
        '                        </tr>\n' +
        '                        <tr>\n' +
        '                          <td colspan="2" height="20"></td>\n' +
        '                        </tr>\n' +
        '                      </tbody>\n' +
        '                    </table>\n' +
        '                    <!-- PROMO column end here -->\n' +
        '                    <!-- FOOTER start here -->\n' +
        '                    <table width="100%" cellpadding="0" cellspacing="0" border="0">\n' +
        '                      <tbody>\n' +
        '                        <tr>\n' +
        '                          <td height="10">&nbsp;</td>\n' +
        '                        </tr>\n' +
        '                        <tr>\n' +
        '                          <td valign="top" align="center"> <span style="font-family: -apple-system,BlinkMacSystemFont,&#39;Segoe UI&#39;,&#39;Roboto&#39;,&#39;Oxygen&#39;,&#39;Ubuntu&#39;,&#39;Cantarell&#39;,&#39;Fira Sans&#39;,&#39;Droid Sans&#39;,&#39;Helvetica Neue&#39;,sans-serif; color:#9EB0C9; font-size:10px;">&copy;\n' +
        '                            <a href="http://opennode-opennode.a3c1.starter-us-west-1.openshiftapps.com/store" target="_blank" style="color:#9EB0C9 !important; text-decoration:none;">OpenNode</a> 2018\n' +
        '                          </span>\n' +
        '\n' +
        '                          </td>\n' +
        '                        </tr>\n' +
        '                        <tr>\n' +
        '                          <td height="50">&nbsp;</td>\n' +
        '                        </tr>\n' +
        '                      </tbody>\n' +
        '                    </table>\n' +
        '                    <!-- FOOTER end here -->\n' +
        '                  </td>\n' +
        '                </tr>\n' +
        '              </tbody>\n' +
        '            </table>\n' +
        '          </td>\n' +
        '        </tr>\n' +
        '      </tbody>\n' +
        '    </table>\n' +
        '  </body>\n' +
        '\n' +
        '</html>',
    emailResetTemplate = (reset_href) => '' +
        '<!DOCTYPE html>\n' +
        '<html>\n' +
        '<head>\n' +
        '\n' +
        '  <meta charset="utf-8">\n' +
        '  <meta http-equiv="x-ua-compatible" content="ie=edge">\n' +
        '  <title>Password Reset</title>\n' +
        '  <meta name="viewport" content="width=device-width, initial-scale=1">\n' +
        '  <style type="text/css">\n' +
        '  /**\n' +
        '   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.\n' +
        '   */\n' +
        '  @media screen {\n' +
        '    @font-face {\n' +
        '      font-family: \'Source Sans Pro\';\n' +
        '      font-style: normal;\n' +
        '      font-weight: 400;\n' +
        '      src: local(\'Source Sans Pro Regular\'), local(\'SourceSansPro-Regular\'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format(\'woff\');\n' +
        '    }\n' +
        '    @font-face {\n' +
        '      font-family: \'Source Sans Pro\';\n' +
        '      font-style: normal;\n' +
        '      font-weight: 700;\n' +
        '      src: local(\'Source Sans Pro Bold\'), local(\'SourceSansPro-Bold\'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format(\'woff\');\n' +
        '    }\n' +
        '  }\n' +
        '  /**\n' +
        '   * Avoid browser level font resizing.\n' +
        '   * 1. Windows Mobile\n' +
        '   * 2. iOS / OSX\n' +
        '   */\n' +
        '  body,\n' +
        '  table,\n' +
        '  td,\n' +
        '  a {\n' +
        '    -ms-text-size-adjust: 100%; /* 1 */\n' +
        '    -webkit-text-size-adjust: 100%; /* 2 */\n' +
        '  }\n' +
        '  /**\n' +
        '   * Remove extra space added to tables and cells in Outlook.\n' +
        '   */\n' +
        '  table,\n' +
        '  td {\n' +
        '    mso-table-rspace: 0pt;\n' +
        '    mso-table-lspace: 0pt;\n' +
        '  }\n' +
        '  /**\n' +
        '   * Better fluid images in Internet Explorer.\n' +
        '   */\n' +
        '  img {\n' +
        '    -ms-interpolation-mode: bicubic;\n' +
        '  }\n' +
        '  /**\n' +
        '   * Remove blue links for iOS devices.\n' +
        '   */\n' +
        '  a[x-apple-data-detectors] {\n' +
        '    font-family: inherit !important;\n' +
        '    font-size: inherit !important;\n' +
        '    font-weight: inherit !important;\n' +
        '    line-height: inherit !important;\n' +
        '    color: inherit !important;\n' +
        '    text-decoration: none !important;\n' +
        '  }\n' +
        '  /**\n' +
        '   * Fix centering issues in Android 4.4.\n' +
        '   */\n' +
        '  div[style*="margin: 16px 0;"] {\n' +
        '    margin: 0 !important;\n' +
        '  }\n' +
        '  body {\n' +
        '    width: 100% !important;\n' +
        '    height: 100% !important;\n' +
        '    padding: 0 !important;\n' +
        '    margin: 0 !important;\n' +
        '  }\n' +
        '  /**\n' +
        '   * Collapse table borders to avoid space between cells.\n' +
        '   */\n' +
        '  table {\n' +
        '    border-collapse: collapse !important;\n' +
        '  }\n' +
        '  a {\n' +
        '    color: #1a82e2;\n' +
        '  }\n' +
        '  img {\n' +
        '    height: auto;\n' +
        '    line-height: 100%;\n' +
        '    text-decoration: none;\n' +
        '    border: 0;\n' +
        '    outline: none;\n' +
        '  }\n' +
        '  </style>\n' +
        '\n' +
        '</head>\n' +
        '<body style="background-color: #e9ecef;">\n' +
        '\n' +
        '  <!-- start preheader -->\n' +
        '  <div class="preheader" style="display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;">\n' +
        '    A preheader is the short summary text that follows the subject line when an email is viewed in the inbox.\n' +
        '  </div>\n' +
        '  <!-- end preheader -->\n' +
        '\n' +
        '  <!-- start body -->\n' +
        '  <table border="0" cellpadding="0" cellspacing="0" width="100%">\n' +
        '\n' +
        '    <!-- start logo -->\n' +
        '    <tr>\n' +
        '      <td align="center" bgcolor="#e9ecef">\n' +
        '        <!--[if (gte mso 9)|(IE)]>\n' +
        '        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">\n' +
        '        <tr>\n' +
        '        <td align="center" valign="top" width="600">\n' +
        '        <![endif]-->\n' +
        '        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">\n' +
        '          <tr>\n' +
        '            <td align="center" valign="top" style="padding: 36px 24px;">\n' +
        '              <a href="https://sendgrid.com" target="_blank" style="display: inline-block;">\n' +
        '                <img src="https://logo.clearbit.com/nodejs.org" alt="Logo" border="0" width="48" style="display: block; width: 48px; max-width: 48px; min-width: 48px;">\n' +
        '              </a>\n' +
        '            </td>\n' +
        '          </tr>\n' +
        '        </table>\n' +
        '        <!--[if (gte mso 9)|(IE)]>\n' +
        '        </td>\n' +
        '        </tr>\n' +
        '        </table>\n' +
        '        <![endif]-->\n' +
        '      </td>\n' +
        '    </tr>\n' +
        '    <!-- end logo -->\n' +
        '\n' +
        '    <!-- start hero -->\n' +
        '    <tr>\n' +
        '      <td align="center" bgcolor="#e9ecef">\n' +
        '        <!--[if (gte mso 9)|(IE)]>\n' +
        '        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">\n' +
        '        <tr>\n' +
        '        <td align="center" valign="top" width="600">\n' +
        '        <![endif]-->\n' +
        '        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">\n' +
        '          <tr>\n' +
        '            <td align="left" bgcolor="#ffffff" style="padding: 36px 24px 0; font-family: \'Source Sans Pro\', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;">\n' +
        '              <h1 style="margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;">Reset Your Password</h1>\n' +
        '            </td>\n' +
        '          </tr>\n' +
        '        </table>\n' +
        '        <!--[if (gte mso 9)|(IE)]>\n' +
        '        </td>\n' +
        '        </tr>\n' +
        '        </table>\n' +
        '        <![endif]-->\n' +
        '      </td>\n' +
        '    </tr>\n' +
        '    <!-- end hero -->\n' +
        '\n' +
        '    <!-- start copy block -->\n' +
        '    <tr>\n' +
        '      <td align="center" bgcolor="#e9ecef">\n' +
        '        <!--[if (gte mso 9)|(IE)]>\n' +
        '        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">\n' +
        '        <tr>\n' +
        '        <td align="center" valign="top" width="600">\n' +
        '        <![endif]-->\n' +
        '        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">\n' +
        '\n' +
        '          <!-- start copy -->\n' +
        '          <tr>\n' +
        '            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: \'Source Sans Pro\', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">\n' +
        '              <p style="margin: 0;">Tap the button below to reset your customer account password. If you didn\'t request a new password, you can safely delete this email.</p>\n' +
        '            </td>\n' +
        '          </tr>\n' +
        '          <!-- end copy -->\n' +
        '\n' +
        '          <!-- start button -->\n' +
        '          <tr>\n' +
        '            <td align="left" bgcolor="#ffffff">\n' +
        '              <table border="0" cellpadding="0" cellspacing="0" width="100%">\n' +
        '                <tr>\n' +
        '                  <td align="center" bgcolor="#ffffff" style="padding: 12px;">\n' +
        '                    <table border="0" cellpadding="0" cellspacing="0">\n' +
        '                      <tr>\n' +
        '                        <td align="center" bgcolor="#1a82e2" style="border-radius: 6px;">\n' +
        '                          <a href="' + reset_href + '" target="_blank" style="display: inline-block; padding: 16px 36px; font-family: \'Source Sans Pro\', Helvetica, Arial, sans-serif; font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;">Reset Password</a>\n' +
        '                        </td>\n' +
        '                      </tr>\n' +
        '                    </table>\n' +
        '                  </td>\n' +
        '                </tr>\n' +
        '              </table>\n' +
        '            </td>\n' +
        '          </tr>\n' +
        '          <!-- end button -->\n' +
        '\n' +
        '          <!-- start copy -->\n' +
        '          <tr>\n' +
        '            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: \'Source Sans Pro\', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">\n' +
        '              <p style="margin: 0;">If that doesn\'t work, copy and paste the following link in your browser:</p>\n' +
        '              <p style="margin: 0;"><a href="' + reset_href + '" target="_blank">https://same-link-as-button.url/xxx-xxx-xxxx</a></p>\n' +
        '            </td>\n' +
        '          </tr>\n' +
        '          <!-- end copy -->\n' +
        '\n' +
        '          <!-- start copy -->\n' +
        '          <tr>\n' +
        '            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: \'Source Sans Pro\', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf">\n' +
        '              <p style="margin: 0;">Cheers,<br> OpenNode 2018</p>\n' +
        '            </td>\n' +
        '          </tr>\n' +
        '          <!-- end copy -->\n' +
        '\n' +
        '        </table>\n' +
        '        <!--[if (gte mso 9)|(IE)]>\n' +
        '        </td>\n' +
        '        </tr>\n' +
        '        </table>\n' +
        '        <![endif]-->\n' +
        '      </td>\n' +
        '    </tr>\n' +
        '    <!-- end copy block -->\n' +
        '\n' +
        '    <!-- start footer -->\n' +
        '    <tr>\n' +
        '      <td align="center" bgcolor="#e9ecef" style="padding: 24px;">\n' +
        '        <!--[if (gte mso 9)|(IE)]>\n' +
        '        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">\n' +
        '        <tr>\n' +
        '        <td align="center" valign="top" width="600">\n' +
        '        <![endif]-->\n' +
        '        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">\n' +
        '\n' +
        '\n' +
        '\n' +
        '        </table>\n' +
        '        <!--[if (gte mso 9)|(IE)]>\n' +
        '        </td>\n' +
        '        </tr>\n' +
        '        </table>\n' +
        '        <![endif]-->\n' +
        '      </td>\n' +
        '    </tr>\n' +
        '    <!-- end footer -->\n' +
        '\n' +
        '  </table>\n' +
        '  <!-- end body -->\n' +
        '\n' +
        '</body>\n' +
        '</html>',
    emailResetedTemplate = (reset_info) => '' +
        '<!DOCTYPE html>\n' +
        '<html>\n' +
        '<head>\n' +
        '\n' +
        '  <meta charset="utf-8">\n' +
        '  <meta http-equiv="x-ua-compatible" content="ie=edge">\n' +
        '  <title>Password Reset</title>\n' +
        '  <meta name="viewport" content="width=device-width, initial-scale=1">\n' +
        '  <style type="text/css">\n' +
        '  /**\n' +
        '   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.\n' +
        '   */\n' +
        '  @media screen {\n' +
        '    @font-face {\n' +
        '      font-family: \'Source Sans Pro\';\n' +
        '      font-style: normal;\n' +
        '      font-weight: 400;\n' +
        '      src: local(\'Source Sans Pro Regular\'), local(\'SourceSansPro-Regular\'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format(\'woff\');\n' +
        '    }\n' +
        '    @font-face {\n' +
        '      font-family: \'Source Sans Pro\';\n' +
        '      font-style: normal;\n' +
        '      font-weight: 700;\n' +
        '      src: local(\'Source Sans Pro Bold\'), local(\'SourceSansPro-Bold\'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format(\'woff\');\n' +
        '    }\n' +
        '  }\n' +
        '  /**\n' +
        '   * Avoid browser level font resizing.\n' +
        '   * 1. Windows Mobile\n' +
        '   * 2. iOS / OSX\n' +
        '   */\n' +
        '  body,\n' +
        '  table,\n' +
        '  td,\n' +
        '  a {\n' +
        '    -ms-text-size-adjust: 100%; /* 1 */\n' +
        '    -webkit-text-size-adjust: 100%; /* 2 */\n' +
        '  }\n' +
        '  /**\n' +
        '   * Remove extra space added to tables and cells in Outlook.\n' +
        '   */\n' +
        '  table,\n' +
        '  td {\n' +
        '    mso-table-rspace: 0pt;\n' +
        '    mso-table-lspace: 0pt;\n' +
        '  }\n' +
        '  /**\n' +
        '   * Better fluid images in Internet Explorer.\n' +
        '   */\n' +
        '  img {\n' +
        '    -ms-interpolation-mode: bicubic;\n' +
        '  }\n' +
        '  /**\n' +
        '   * Remove blue links for iOS devices.\n' +
        '   */\n' +
        '  a[x-apple-data-detectors] {\n' +
        '    font-family: inherit !important;\n' +
        '    font-size: inherit !important;\n' +
        '    font-weight: inherit !important;\n' +
        '    line-height: inherit !important;\n' +
        '    color: inherit !important;\n' +
        '    text-decoration: none !important;\n' +
        '  }\n' +
        '  /**\n' +
        '   * Fix centering issues in Android 4.4.\n' +
        '   */\n' +
        '  div[style*="margin: 16px 0;"] {\n' +
        '    margin: 0 !important;\n' +
        '  }\n' +
        '  body {\n' +
        '    width: 100% !important;\n' +
        '    height: 100% !important;\n' +
        '    padding: 0 !important;\n' +
        '    margin: 0 !important;\n' +
        '  }\n' +
        '  /**\n' +
        '   * Collapse table borders to avoid space between cells.\n' +
        '   */\n' +
        '  table {\n' +
        '    border-collapse: collapse !important;\n' +
        '  }\n' +
        '  a {\n' +
        '    color: #1a82e2;\n' +
        '  }\n' +
        '  img {\n' +
        '    height: auto;\n' +
        '    line-height: 100%;\n' +
        '    text-decoration: none;\n' +
        '    border: 0;\n' +
        '    outline: none;\n' +
        '  }\n' +
        '  </style>\n' +
        '\n' +
        '</head>\n' +
        '<body style="background-color: #e9ecef;">\n' +
        '\n' +
        '  <!-- start preheader -->\n' +
        '  <div class="preheader" style="display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;">\n' +
        '    A preheader is the short summary text that follows the subject line when an email is viewed in the inbox.\n' +
        '  </div>\n' +
        '  <!-- end preheader -->\n' +
        '\n' +
        '  <!-- start body -->\n' +
        '  <table border="0" cellpadding="0" cellspacing="0" width="100%">\n' +
        '\n' +
        '    <!-- start logo -->\n' +
        '    <tr>\n' +
        '      <td align="center" bgcolor="#e9ecef">\n' +
        '        <!--[if (gte mso 9)|(IE)]>\n' +
        '        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">\n' +
        '        <tr>\n' +
        '        <td align="center" valign="top" width="600">\n' +
        '        <![endif]-->\n' +
        '        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">\n' +
        '          <tr>\n' +
        '            <td align="center" valign="top" style="padding: 36px 24px;">\n' +
        '              <a href="https://sendgrid.com" target="_blank" style="display: inline-block;">\n' +
        '                <img src="https://logo.clearbit.com/nodejs.org" alt="Logo" border="0" width="48" style="display: block; width: 48px; max-width: 48px; min-width: 48px;">\n' +
        '              </a>\n' +
        '            </td>\n' +
        '          </tr>\n' +
        '        </table>\n' +
        '        <!--[if (gte mso 9)|(IE)]>\n' +
        '        </td>\n' +
        '        </tr>\n' +
        '        </table>\n' +
        '        <![endif]-->\n' +
        '      </td>\n' +
        '    </tr>\n' +
        '    <!-- end logo -->\n' +
        '\n' +
        '    <!-- start hero -->\n' +
        '    <tr>\n' +
        '      <td align="center" bgcolor="#e9ecef">\n' +
        '        <!--[if (gte mso 9)|(IE)]>\n' +
        '        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">\n' +
        '        <tr>\n' +
        '        <td align="center" valign="top" width="600">\n' +
        '        <![endif]-->\n' +
        '        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">\n' +
        '          <tr>\n' +
        '            <td align="left" bgcolor="#ffffff" style="padding: 36px 24px 0; font-family: \'Source Sans Pro\', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;">\n' +
        '              <h1 style="margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;">Your Password was Changed</h1>\n' +
        '            </td>\n' +
        '          </tr>\n' +
        '        </table>\n' +
        '        <!--[if (gte mso 9)|(IE)]>\n' +
        '        </td>\n' +
        '        </tr>\n' +
        '        </table>\n' +
        '        <![endif]-->\n' +
        '      </td>\n' +
        '    </tr>\n' +
        '    <!-- end hero -->\n' +
        '\n' +
        '    <!-- start copy block -->\n' +
        '    <tr>\n' +
        '      <td align="center" bgcolor="#e9ecef">\n' +
        '        <!--[if (gte mso 9)|(IE)]>\n' +
        '        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">\n' +
        '        <tr>\n' +
        '        <td align="center" valign="top" width="600">\n' +
        '        <![endif]-->\n' +
        '        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">\n' +
        '\n' +
        '          <!-- start copy -->\n' +
        '          <tr>\n' +
        '            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: \'Source Sans Pro\', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">\n' +
        '              <p style="margin: 0;"> ' + reset_info + '.</p>\n' +
        '            </td>\n' +
        '          </tr>\n' +
        '          <!-- end copy -->\n' +
        '\n' +
        '          <!-- start button -->\n' +
        '          <tr>\n' +
        '            <td align="left" bgcolor="#ffffff">\n' +
        '              <table border="0" cellpadding="0" cellspacing="0" width="100%">\n' +
        '                <tr>\n' +
        '                  <td align="center" bgcolor="#ffffff" style="padding: 12px;">\n' +
        '                    <table border="0" cellpadding="0" cellspacing="0">\n' +
        '                      <tr>\n' +
        '                        <td align="center" bgcolor="#1a82e2" style="border-radius: 6px;">\n' +
        '                         \n' +
        '                        </td>\n' +
        '                      </tr>\n' +
        '                    </table>\n' +
        '                  </td>\n' +
        '                </tr>\n' +
        '              </table>\n' +
        '            </td>\n' +
        '          </tr>\n' +
        '          <!-- end button -->\n' +
        '\n' +
        '          <!-- start copy -->\n' +
        '          <tr>\n' +
        '            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: \'Source Sans Pro\', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">\n' +
        '              <p style="margin: 0;"></p>\n' +
        '              <p style="margin: 0;"></p>\n' +
        '            </td>\n' +
        '          </tr>\n' +
        '          <!-- end copy -->\n' +
        '\n' +
        '          <!-- start copy -->\n' +
        '          <tr>\n' +
        '            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: \'Source Sans Pro\', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf">\n' +
        '              <p style="margin: 0;">Cheers,<br> OpenNode 2018</p>\n' +
        '            </td>\n' +
        '          </tr>\n' +
        '          <!-- end copy -->\n' +
        '\n' +
        '        </table>\n' +
        '        <!--[if (gte mso 9)|(IE)]>\n' +
        '        </td>\n' +
        '        </tr>\n' +
        '        </table>\n' +
        '        <![endif]-->\n' +
        '      </td>\n' +
        '    </tr>\n' +
        '    <!-- end copy block -->\n' +
        '\n' +
        '    <!-- start footer -->\n' +
        '    <tr>\n' +
        '      <td align="center" bgcolor="#e9ecef" style="padding: 24px;">\n' +
        '        <!--[if (gte mso 9)|(IE)]>\n' +
        '        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">\n' +
        '        <tr>\n' +
        '        <td align="center" valign="top" width="600">\n' +
        '        <![endif]-->\n' +
        '        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">\n' +
        '\n' +
        '\n' +
        '\n' +
        '        </table>\n' +
        '        <!--[if (gte mso 9)|(IE)]>\n' +
        '        </td>\n' +
        '        </tr>\n' +
        '        </table>\n' +
        '        <![endif]-->\n' +
        '      </td>\n' +
        '    </tr>\n' +
        '    <!-- end footer -->\n' +
        '\n' +
        '  </table>\n' +
        '  <!-- end body -->\n' +
        '\n' +
        '</body>\n' +
        '</html>',
    emailWishListTemplate = (product) => '' +
        '\n' +
        '<html>\n' +
        '    <head>\n' +
        '        <title> </title>\n' +
        '      \n' +
        '<!-- Headers to prevent the caching of pages -->\n' +
        '<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>\n' +
        '<meta http-equiv="pragma" content="no-cache"/>\n' +
        '<meta http-equiv="cache-control" content="no-store"/>\n' +
        '<script type="text/javascript" src="/js/lightbox_js.jsp?b=1.66.0.53"></script>\n' +
        '</head>\n' +
        '    <body style="margin: 0;">\n' +
        '\n' +
        '    <!DOCTYPE HTML>\n' +
        '<html>\n' +
        '<head>\n' +
        '</head><body>\n' +
        '<div id="mainContent" style="text-align: left;">\n' +
        '<table cellpadding="10" cellspacing="0" style="background-color: rgb(211,216,221); width: 100%; height: 100%;">\n' +
        '<tbody>\n' +
        '<tr>\n' +
        '<td valign="top">\n' +
        '<table align="center" cellpadding="0" cellspacing="0">\n' +
        '<tbody>\n' +
        '<tr>\n' +
        '<td>\n' +
        '<table cellpadding="0" cellspacing="0" style="width: 600px;">\n' +
        '<tbody>\n' +
        '<tr>\n' +
        '<td sectionid="preheader" style="text-align: left; margin: 0; padding: 10px 0; border: none; white-space: ' +
        'normal; line-height: normal;">\n' +
        '<div>\n' +
        '<div>\n' +
        '<div style="font-size: 11px; font-family: arial; color: rgb(26,36,46); margin: 0; padding: 0; background: none;' +
        ' border: none; white-space: normal; line-height: normal; overflow: visible; text-align: center;">\n' +
        '<div contentid="preheader" style="font-size: 11px; font-family: arial; color: rgb(26,36,46); margin: 0; ' +
        'padding: 0; background: none; border: none; white-space: normal; line-height: normal; overflow: visible; text-align: center;">\n' +
        '<div>\n' +
        ' \n' +
        '</div>\n' +
        '</div>\n' +
        '</div>\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '<div style="height: 15px; line-height: 15px;">\n' +
        '&nbsp;\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '<div style="height: 15px; line-height: 15px;">\n' +
        '&nbsp;\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '<div contentid="image">\n' +
        '<div contentid="image">\n' +
        '<div style="text-align: center;"> \n' +
        '</div>\n' +
        '</div>\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '<div style="text-align: center;font-size:19px;font-family:Comic Sans MS", cursive, sans-serif;">\n' +
        'New item in wishlist  \n' +
        '  </div\n' +
        '</div>\n' +
        '<div>\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '<div style="height: 25px; line-height: 25px;">\n' +
        '&nbsp;\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '<div contentid="image">\n' +
        '<div contentid="image">\n' +
        '<div style="text-align: center;">\n' +
        '<a href="'+product.urlToProduct+'" nottracked="false" ' +
        'shape="rect" style="color: rgb(6,69,173);">' +
        '<img align="bottom" alt="Matias 40cm Clock $74.95" border="0"' +
        ' height="202" src="'+product.productImageUrl+'" ' +
        'style="margin: 0; margin-right: 0px; margin-left: 0px; padding: 0; background: none; border: none;' +
        ' white-space: normal; line-height: normal;" title="'+product.name+' $'+product.price+'" width="600" /></a>\n' +
        '</div>\n' +
        '</div>\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '<div style="height: 15px; line-height: 15px;">\n' +
        '&nbsp;\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '<div style="height: 15px; line-height: 15px;">\n' +
        '&nbsp;\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '<div style="height: 15px; line-height: 15px;">\n' +
        '&nbsp;\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '<div style="height: 15px; line-height: 15px;">\n' +
        '&nbsp;\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '</div>\n' +
        '</div>\n' +
        '</td>\n' +
        '</tr>\n' +
        '</tbody>\n' +
        '</table>\n' +
        '</td>\n' +
        '</tr>\n' +
        '<tr>\n' +
        '<td>\n' +
        '<table bgcolor="#FFFFFF" cellpadding="20" cellspacing="0" style="width: 600px; background-color: rgb(255,255,255);">\n' +
        '<tbody>\n' +
        '<tr>\n' +
        '<td sectionid="body" style="background-color: rgb(211,216,221); text-align: left; margin: 0; padding: 20px;' +
        ' border: none; white-space: normal; line-height: normal;" valign="top">\n' +
        '<div>\n' +
        '<div style="font-size: 12px; font-family: arial; color: rgb(0,0,0); margin: 0; padding: 0;' +
        ' background: none; border: none; white-space: normal; line-height: normal; overflow: visible;">\n' +
        '<div contentid="paragraph" style="font-size: 12px; font-family: arial; color: rgb(0,0,0); ' +
        'margin: 0; padding: 0; background: none; border: none; white-space: normal; line-height: normal; overflow: visible; text-align: center;">\n' +
        '<div style="overflow: visible;">\n' +
        '<p style="font-size: 12px; font-family: arial; margin: 0; color: null;">\n' +
        '&nbsp;\n' +
        '</p>\n' +
        '<div style="overflow: visible;">\n' +
        '&nbsp;\n' +
        '</div>\n' +
        '</div>\n' +
        '</div>\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '<div style="height: 15px; line-height: 15px;">\n' +
        '&nbsp;\n' +
        '</div>\n' +
        '</div>\n' +
        '<div>\n' +
        '<div style="font-size: 12px; font-family: arial; color: rgb(0,0,0); margin: 0; padding: 0; background:' +
        ' none; border: none; white-space: normal; line-height: normal; overflow: visible;">\n' +
        '<div contentid="paragraph" style="font-size: 12px; font-family: arial; color: rgb(0,0,0); margin:' +
        ' 0; padding: 0; background: none; border: none; white-space: normal; line-height: normal; overflow: visible;">\n' +
        '<div style="overflow: visible; text-align: center;">\n' +
        '\n' +
        '</div>\n' +
        '</div>\n' +
        '</div>\n' +
        '</div>\n' +
        '</td>\n' +
        '</tr>\n' +
        '<tr>\n' +
        '<td sectionid="footer" style="background-color: rgb(211,216,221); text-align: left; margin: 0;' +
        ' padding: 20px; border: none; white-space: normal; line-height: normal;" valign="top">\n' +
        '<div>\n' +
        '<div style="font-size: 10px; font-family: verdana; color: rgb(0,0,0); margin: 0; padding: 0;' +
        ' background: none; border: none; white-space: normal; line-height: normal; overflow: visible;text-align:center;">\n' +
        '<a href="http://opennode-opennode.a3c1.starter-us-west-1.openshiftapps.com">OpenNode</a>\n' +
        '</div>\n' +
        '</div>\n' +
        '</td>\n' +
        '</tr>\n' +
        '</tbody>\n' +
        '</table>\n' +
        '</td>\n' +
        '</tr>\n' +
        '</tbody>\n' +
        '</table>\n' +
        '</td>\n' +
        '</tr>\n' +
        '</tbody>\n' +
        '</table>\n' +
        '</div>\n' +
        '</body>\n' +
        '</html></body>\n' +
        '</html>\n'