module.exports =(app) => {
    app.get('/user/cart',app.controllers.cart.cartPage)

    app.post('/store/:category/:exact_category/add-to-cart/:id',app.controllers.cart.addToCart)
    app.post('/store/:category/:exact_category/:product_url/add-to-cart/:id',app.controllers.cart.addToCart)
    app.post('/user/cart/add-to-cart/:id',app.controllers.cart.addToCart)
    app.get('/search/add-to-cart',app.controllers.cart.searchAddToCart)

    app.post('/user/cart/subtract-cart/:id',app.controllers.cart.subtractCart)

    app.post('/user/cart/cart-remove/:id',app.controllers.cart.removeCart)
}