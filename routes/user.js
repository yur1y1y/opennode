
module.exports = (app) => {
    app.get('/user',app.controllers.user.profilePage)

    app.get('/user/signup',app.controllers.user.signupPage);
    app.post('/user/signup',app.controllers.user.signupLocal);

    app.get('/user/signin',app.controllers.user.signInPage);
    app.post('/user/signin',app.controllers.user.signinLocal);

    app.get('/logout',app.controllers.user.logout)
    app.post('/user/update-profile',app.controllers.user.updateProfile)

    app.get('/user/verify/:token',app.controllers.user.verifyUser)
    app.get('/user/resend',app.controllers.user.resendToken)

    app.get('/user/forgot',app.controllers.user.forgotPasswordPage)
    app.post('/user/forgot-password',app.controllers.user.forgotPassword)

    app.get('/user/reset-password/:passwordToken',app.controllers.user.resetPasswordPage)
    app.post('/user/reset-password',app.controllers.user.resetPassword)

    app.post('/add-to-wishlist',app.controllers.user.toWishList)
    app.get('/user/wishlist',app.controllers.user.wishPage)
    app.post('/user/remove-viewed',app.controllers.user.removeViewed)

}