module.exports =(app) => {
    app.get('/store',app.controllers.category.rootCategories)

    app.get('/store/:category',app.controllers.category.subCategories)

    app.get('/store/:category/:exact_category',app.controllers.category.exactCategory)
}